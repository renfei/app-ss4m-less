/*
 * Copyright (c) 2018-2025, Sven Augustus (svenaugustus@outlook.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package io.flysium.framework.exception;

import io.flysium.framework.message.CodeInfo;

/**
 * 抛出包含错误编码的异常
 * 
 * @author SvenAugustus(蔡政滦) e-mail: SvenAugustus@outlook.com
 * @date 1.0
 */
public class ErrorHelper {

	private ErrorHelper() {
	}

	/**
	 * 抛出异常
	 * 
	 * @param code
	 * @param message
	 */
	public static void throwError(String code, String message) {
		throw new BizException(code, message);
	}

	/**
	 * 抛出异常
	 * 
	 * @param codeInfo
	 */
	public static void throwError(CodeInfo codeInfo) {
		throw new BizException(codeInfo);
	}
}