/*
 * Copyright (c) 2018-2025, Sven Augustus (svenaugustus@outlook.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.common.dao.ILogDAO;
import demo.common.service.ILogService;
import demo.common.vo.SysOperLog;
import io.flysium.framework.transaction.annotation.LogTransactional;
import io.flysium.framework.vo.PageModel;

/**
 * 系统操作日志服务
 * 
 * @author SvenAugustus(蔡政滦) e-mail: SvenAugustus@outlook.com
 * @version 2017年3月6日
 */
@Service
public class LogService implements ILogService {

	@Autowired
	private ILogDAO logDAO;

	@Override
	@LogTransactional
	public PageModel<SysOperLog> queryLogByUserId(String userId) {
		// 如果可能，在此做业务逻辑判断，业务逻辑处理。
		return logDAO.queryLogByUserId(userId);
	}
}
