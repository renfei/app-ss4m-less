/*
 * Copyright (c) 2018-2025, Sven Augustus (svenaugustus@outlook.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package demo.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import demo.common.service.IUserService;
import demo.common.vo.User;
import io.flysium.framework.Consts;
import io.flysium.framework.message.ResponseResult;
import io.flysium.framework.vo.PageModel;

/**
 * 用户Controller
 * 
 * @author SvenAugustus(蔡政滦) e-mail: SvenAugustus@outlook.com
 * @version 2017年3月6日
 */
@RestController // 标识为restful风格，适合回参不跳转页面的ajax模式
@RequestMapping
public class UserController {

	@Autowired
	private IUserService userService;

	/**
	 * 查询用户信息
	 * 
	 * @param params
	 * @return
	 */
	@RequestMapping
	public ResponseResult queryUserInfo(@RequestBody Map<String, Object> params) {
		ResponseResult responseResult = new ResponseResult(Consts.CodeInfoSet.CODE_00000);
		PageModel<User> pageModel = userService.queryUser(params);
		responseResult.setResult(pageModel);
		return responseResult;
	}

}
