/*
 * Copyright (c) 2018-2025, Sven Augustus (svenaugustus@outlook.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import demo.common.vo.DemoVO;

/**
 * 一个简单的Controller样例（ViewResolver + ModelAndView + JSP + EL表达式)
 * 
 * @author SvenAugustus(蔡政滦) e-mail: SvenAugustus@outlook.com
 * @version 2017年3月2日
 */
@Controller // 一般mvc模式
@RequestMapping("/demo/classic")
public class ClassicDemoController {

	/**
	 * 测试方法：简单输出欢迎用语
	 * 
	 * @param vo
	 * @return
	 */
	@RequestMapping(value = "/hi", method = RequestMethod.POST)
	public ModelAndView sayHi(DemoVO vo) {
		ModelAndView view = new ModelAndView();
		view.setViewName("/demo/classic/hi"); // 跳转页面： 前缀+/demo/classic/hi +后缀
		view.addObject("message", vo.getName());
		return view;
	}

}
