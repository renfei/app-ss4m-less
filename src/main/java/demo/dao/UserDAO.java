/*
 * Copyright (c) 2018-2025, Sven Augustus (svenaugustus@outlook.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package demo.dao;

import java.util.Map;

import org.springframework.stereotype.Repository;

import demo.common.dao.IUserDAO;
import demo.common.vo.User;
import io.flysium.framework.util.jdbc.DaoUtils;
import io.flysium.framework.vo.PageModel;

/**
 * 用户DAO
 * 
 * @author SvenAugustus(蔡政滦) e-mail: SvenAugustus@outlook.com
 * @version 2017年3月8日
 */
@Repository // DAO层注解
public class UserDAO implements IUserDAO {

	/**
	 * 查询用户信息
	 */
	@Override
	public PageModel<User> queryUser(Map params) {
		PageModel<User> pageModel = DaoUtils.selectPageModel("User.queryUserByUserName", params);
		return pageModel;
	}

}
